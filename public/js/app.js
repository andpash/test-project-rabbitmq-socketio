var socket = io();
var apiBasePath = '/api/v1/';
var appendLogMessage = message => {
  $('#log').append(message + '<br />');
};

$('#send-request').on('click', event => {
  event.preventDefault();

  $.ajax({
    method: 'POST',
    url: apiBasePath + 'request',
    data: { },
    success: result => {
      appendLogMessage(result.message);
    }
  });
});

socket.on('log', message => {
  appendLogMessage(message);
});