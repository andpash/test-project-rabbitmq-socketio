const port = 3000;
const apiBasePath = '/api/v1/';

module.exports = {
  port,
  timeout: 10000,
  amqp: {
    connection: 'amqp://localhost',
    queueName: 'tasks',
  },
  thirdPartyServiceUrl: `http://localhost:${port}${apiBasePath}`,
  apiBasePath,
}