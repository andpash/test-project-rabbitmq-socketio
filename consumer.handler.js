const axios = require('axios');
const config = require('./config');
let failRequest = true;

module.exports = io => {
  return async (message) => {
    let isFailed = false;
    // Send a request to a third-party service
    try {
      if (failRequest) {
        failRequest = false;
        await axios.post(`${config.thirdPartyServiceUrl}mock-api-call-fail`, {}, { timeout: config.timeout });
  
        
      } else {
        failRequest = true;
        await axios.post(`${config.thirdPartyServiceUrl}mock-api-call-success`, {});
      }
      
    } catch (error) {
      isFailed = true;
      io.emit('log', `Request "${message}" wasn't processed`);
    }
  
    if (!isFailed) {
      io.emit('log', `Request "${message}" was processed`);
    }
  };
};
 