const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const config = require('./config');
const path = require('path');
const bodyParser = require('body-parser');
const Queue = require('./queue');
const queue = new Queue(config);
const { Server } = require('socket.io');

const mockApiCallFailRoute = require('./routes/mock-api-call-fail.route');
const mockApiCallSuccessRoute = require('./routes/mock-api-call-success.route');
const staticRoute = require('./routes/static.route');
const errorHandler = require('./controllers/error-handler.controler');
const requestRoute = require('./routes/request.route')(queue);
const consumerHandler = require('./consumer.handler')(new Server(server));

// Setup routes
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'pug');
app.set('views', './views');
// Mock routes for emulate succees and fail response from third-party service
app.use(mockApiCallFailRoute);
app.use(mockApiCallSuccessRoute);

app.use(requestRoute);
app.use(staticRoute);
app.use(errorHandler);

Promise.all([
  queue.initConsumer(consumerHandler)
])
  .then(() => {
    server.listen(config.port);
  })
  .catch(console.error);
