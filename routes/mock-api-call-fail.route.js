const express = require('express');
const router = express.Router();
const getMockResponse = require('../controllers/mock-api-call-fail.controler');
const config = require('../config');

router
  .route(`${config.apiBasePath}mock-api-call-fail`)
  .post(getMockResponse);

module.exports = router;
