const express = require('express');
const router = express.Router();
const getMockResponse = require('../controllers/mock-api-call-success.controler');
const config = require('../config');

router
  .route(`${config.apiBasePath}mock-api-call-success`)
  .post(getMockResponse);

module.exports = router;
