const express = require('express');
const router = express.Router();
const handleRequest = require('../controllers/request.controler');
const config = require('../config');

module.exports = queue => {
  router
    .route(`${config.apiBasePath}request`)
    .post(handleRequest(queue));

  return router;
};
