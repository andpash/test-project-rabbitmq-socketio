const amqp = require('amqplib');
let amqpConnection;

class Queue {
  constructor(config) {
    this.connect = amqp.connect(config.amqp.connection);
    this.queueName = config.amqp.queueName;
  }

  async getConnection() {
    if (amqpConnection) {
      return amqpConnection;
    }

    amqpConnection = await this.connect;

    return amqpConnection;
  }

  async initConsumer(consumerHandler) {
    if (typeof consumerHandler !== 'function') {
      throw new Error('Please provide consumer handler function');
    }

    const connect = await this.getConnection();
    const channel = await connect.createChannel();
    await channel.assertQueue(this.queueName);
    
    return channel.consume(this.queueName, async msg => {
      if (msg === null) {
        return null;
      }
    
      const message = msg.content.toString();

      try {
        await consumerHandler(message);
        channel.ack(msg);
      } catch (error) {
        console.error(error);
      }
      
      return msg;
    });
  }

  async publish(payload) {
    const connect = await this.getConnection();
    const channel = await connect.createChannel();
  
    await channel.assertQueue(this.queueName);
    await channel.sendToQueue(this.queueName, Buffer.from(payload));

    return true;
  }
}

module.exports = Queue;
