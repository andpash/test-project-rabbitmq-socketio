const { v4: uuidv4 } = require('uuid');

module.exports = queue => {
  return async (req, res) => {
    const requestId = uuidv4();

    await queue.publish(requestId);

    res.json({ message: `Request "${requestId}" was created`});
  }
};
