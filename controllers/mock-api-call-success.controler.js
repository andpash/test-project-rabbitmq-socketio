const { sleep } = require('../helper');

module.exports = async (req, res) => {
  // Emulate response in 2 seconds
  await sleep(2000);

  res.json({ success: true });
};
