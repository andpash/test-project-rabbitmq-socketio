const { sleep } = require('../helper');
const config = require('../config');

module.exports = async (req, res) => {
  // Emulate repsonse in more than 10 seconds
  await sleep(config.timeout + 10);
  
  res.json({ success: true });
};
