install: npm install

install rabbitmq: docker run -d --hostname my-rabbit --name some-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management

run: node index.js

open: http://localhost:3000/
